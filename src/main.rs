use std::fs::{self, File};
use std::io::BufReader;
use std::io::prelude::*;
use std::time::Instant;
use std::sync::{Mutex, Arc};
use clap::{Arg, App};
use crossbeam::thread;

mod liste_mot;
mod helpers;

fn main() {
    let matches = App::new("Markov parser")
        .version("1.0")
        .author("Gabriel Goffart")
        .about("Permet de parser un texte et d'écrire une chaine de Markov")
        .arg(Arg::with_name("auteur")
            .short("a")
            .long("auteur")
            .value_name("AUTEUR")
            .required_unless("all")
            .conflicts_with("all")
            .help("Le nom de l'auteur")
            .takes_value(true))
        .arg(Arg::with_name("n-gramme")
            .short("n")
            .long("n-gramme")
            .value_name("NOMBRE")
            .required(true)
            .takes_value(true)
            .help("la taille des n-grammes à trouver"))
        .arg(Arg::with_name("all")
            .short("A")
            .long("all")
            .required_unless("auteur")
            .conflicts_with("auteur")
            .takes_value(false)
            .help("spécifie tous les auteurs dans le dossier Texts/"))
        .get_matches();
    
    let mut authors: Vec<String> = vec![];

    if matches.is_present("all") {
        authors = helpers::get_all_dirs();
    } else {
        authors = vec![String::from(matches.value_of("auteur").unwrap())];
    }

    let results: Vec<Vec<liste_mot::Mot>> = vec![];
    let mutex = Arc::new(Mutex::from(results));

    let ngram: usize = matches.value_of("n-gramme").unwrap_or("1").parse().unwrap_or(1);
        
    let total_time = Instant::now();

    thread::scope(|s| {
        for author in authors.iter() {
            let mutex = Arc::clone(&mutex);
            s.spawn(move |_| {
                let time = Instant::now();
                let result = parse_text(&author, &ngram);
                let mut res = mutex.lock().unwrap();
                res.push(result);
                println!("Done parsing author :\t{}. Took {} milli-seconds.", author, time.elapsed().as_millis());
            });
        }
    }).unwrap();

    let results = mutex.lock().unwrap();

    println!("Done parsing all authors. Took {} milli-seconds.", total_time.elapsed().as_millis());

    for i in 0..results.len() {
        println!("Le n-gramme de longueur {} de l'auteur {} le plus utilisé est \"{}\", {} fois.", ngram, authors[i], results[i][0].value, results[i][0].count);
    }
}

fn parse_text(author: &str, ngram_length: &usize) -> Vec<liste_mot::Mot> {

    let mut liste = liste_mot::ListeMot::new(author);
    let mut ngram: Vec<String> = vec![];
    let mut path_name = String::from("Texts/");
    path_name.push_str(author);
    path_name.push('/');
    let paths = fs::read_dir(path_name).unwrap();

    for entry in paths {
        let path = entry.unwrap().path();
        let file = File::open(path.clone()).expect("File not found. ");

        let file = BufReader::new(file);

        println!("\tParsing text: {}",path.display());

        for line in file.lines() {

            let line: String = line.unwrap();
            let line = helpers::remove_punctuation(line);

            for word in line.split_whitespace() {
                if word.len() < 3  && *ngram_length < 3{
                    continue;
                }
                
                ngram.push(String::from(word));

                if ngram.len() == *ngram_length {
                    let mut s = String::new();

                    for w in ngram.iter() {
                        s.push_str(w);
                        s.push(' ');
                    }
                    liste.insert(liste_mot::Mot::new(s.trim_end()));

                    ngram.remove(0);
                }

            }
        }
    }

    liste.sort()
}