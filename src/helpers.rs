
use std::fs;

const PUNC: &str = ",.[]{}!?\\/();:'\"-_";

pub fn remove_punctuation(line: String) -> String {
    let mut new_line = String::new();

    for c in line.to_ascii_lowercase().chars() {
        if PUNC.contains(c) {
            new_line.push(' ');
        } else {
            new_line.push(c);
        }
    }

    new_line
}

pub fn get_all_dirs() -> Vec<String> {

    let paths = fs::read_dir("Texts/").unwrap();

    let mut authors: Vec<String> = vec![];

    for path in paths {
        let path = path.unwrap();
        authors.push(String::from(path.path().file_name().unwrap().to_str().unwrap()));
    }

    authors
}