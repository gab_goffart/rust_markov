
use std::collections::HashMap;


#[derive(Hash, Clone, Debug)]
pub struct Mot {
	pub value: String,
	pub count: i32,
}

impl Mot {
	pub fn new(value: &str) -> Mot {
		Mot{value: String::from(value), count: 1}
	}
}

impl Eq for Mot {

}

impl PartialEq for Mot {
	fn eq(&self, other: &Self) -> bool {
		self.value == other.value
	}
}


#[derive(Debug)]
pub struct ListeMot {
	mots: HashMap<String, Mot>,
	auteur: String,
}

impl ListeMot {
	pub fn new(auteur: &str) -> ListeMot {
		ListeMot {auteur: String::from(auteur), mots: HashMap::new()}
	}

	pub fn insert(&mut self, mot: Mot) {
		let mot2 = self.mots.get_mut(&mot.value);
		
		match mot2 {
			Some(x) => { x.count += 1; },
			None => { self.mots.insert(mot.value.clone(), mot); }
		}
	}

	pub fn sort(&self) -> Vec<Mot> {
		let mut mots: Vec<Mot> = self.mots.iter().map(|(_, val)| val.clone()).collect();

		mots.sort_by(|a: &Mot, b: &Mot| b.count.cmp(&a.count));

		mots
	}

	pub fn get_auteur(&self) -> String {
		self.auteur.clone()
	}
}