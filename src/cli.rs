use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(name = "Markov_generator", about = "Generation de chaine de markov et parsing de texte")]
pub struct Cli {
    #[structopt(short = "a", long = "author")]
    pub author: String,
    #[structopt(short = "n", long = "n-gram")]
    pub ngram: usize,
}