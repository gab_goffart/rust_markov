# CHAINE DE MARKOV
Ceci est un programme simple qui lis des textes et permet de donner le nombre d'occurence des mots les plus utilisés. Le n-gramme est variable, hardcode dans le code pour l'instant. Dans les versions futures, il sera possible de passer le n-gramme en paramètre et de générer une chaine de Markov à la façon de l'auteur choisi. 

## Comment éxécuter le programme
- Installer [rustup](https://rustup.rs/)
- Installer Rust avec l'aide de l'utilitaire rustup
	- `rustup install toolchain stable`
- `git clone https://bitbucket.org/gab_goffart/rust_markov.git`
- `cd rust_markov`
- `cargo build --release`
- `./target/release/markov`